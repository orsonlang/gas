prefix = /usr/local
bindir = $(prefix)/bin
mandir = $(prefix)/man/man1

.PHONY: all
all: gas

gas:
	orson gas.os
	mv a.out gas
